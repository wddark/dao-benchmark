package com.ibeetl.dao.common;

import com.ibeetl.dao.think.service.ThinkPerformaceTestService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ibeetl.dao.beetlsql.service.BeetlSqlPerformaceTestService;
import com.ibeetl.dao.jdbc.RawJDBCPerformaceTestService;
import com.ibeetl.dao.jpa.service.JpaPerformaceTestService;
import com.ibeetl.dao.mybatis.service.MyBatisPerformanceTestService;

@Configuration
public class TestServiceConfig {
    @Bean
    @ConditionalOnProperty(name = "test.target", havingValue = "beetlsql")
    public TestServiceInterface getBeetlSqlTestService() {
        return new BeetlSqlPerformaceTestService();
    }


    @Bean
    @ConditionalOnProperty(name = "test.target", havingValue = "jpa")
    public TestServiceInterface getJpaTestService() {
        return new JpaPerformaceTestService();
    }

    @Bean
    @ConditionalOnProperty(name = "test.target", havingValue = "mybatis")
    public TestServiceInterface getMyBatisTestService() {
        return new MyBatisPerformanceTestService();
    }
    
    @Bean
    @ConditionalOnProperty(name = "test.target", havingValue = "jdbc")
    public TestServiceInterface getRawJDBCTestService() {
        return new RawJDBCPerformaceTestService();
    }

    @Bean
    @ConditionalOnProperty(name = "test.target", havingValue = "think")
    public TestServiceInterface getThinkTestService() {
        return new ThinkPerformaceTestService();
    }

}
